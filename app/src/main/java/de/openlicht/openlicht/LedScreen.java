package de.openlicht.openlicht;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import net.margaritov.preference.colorpicker.ColorPickerDialog;

/**
 * Created by darke on 13.02.2018.
 */

public class LedScreen extends Activity{

    ImageButton led1, led2, led3, led4, led5, allLeds;
    Button takePicture;
    private ColorPickerDialog colorPickerDialog;
    private BluetoothConnectionService btService;

    private int color = 0;
    private int[] colors = new int[5];

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        btService = MainActivity.mBluetoothConnection;
        setContentView(R.layout.led_layout);

        takePicture = findViewById(R.id.cameraBtn);
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LedScreen.this, PictureScreen.class);

                intent.putExtra("color", color);
                startActivityForResult(intent, 5);
            }
        });

        led1 = findViewById(R.id.led1);
        led2 = findViewById(R.id.led2);
        led3 = findViewById(R.id.led3);
        led4 = findViewById(R.id.led4);
        led5 = findViewById(R.id.led5);
        allLeds = findViewById(R.id.all_led);

        led1.setOnClickListener(ledListener);
        led2.setOnClickListener(ledListener);
        led3.setOnClickListener(ledListener);
        led4.setOnClickListener(ledListener);
        led5.setOnClickListener(ledListener);
        allLeds.setOnClickListener(ledListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        colors = data.getExtras().getIntArray("color");
        led1.setBackgroundColor(colors[0]);
        led2.setBackgroundColor(colors[1]);
        led3.setBackgroundColor(colors[2]);
        led4.setBackgroundColor(colors[3]);
        led5.setBackgroundColor(colors[4]);

        String[] leds = {"led1", "led2", "led3", "led4", "led5"};

        if(btService != null) btService.writeColor(colors[0], colors[1], colors[2], colors[3], colors[4]);
//        for(int i = 0; i < colors.length; i++){
//            Log.d("COLORS", "C: " + colors[i]);
//            if(btService != null){
//                String bytes = Integer.toHexString(colors[i]);
//                byte[] bytesToSend = hexStringToByte(bytes);
//                btService.writeColor(leds[i], bytesToSend);
//                Log.d("BT", "write bytes to microcontroller: " + leds[i]);
//            }
//        }

    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        MainActivity.mBluetoothConnection.disconnect();
        MainActivity.mBluetoothConnection = null;
        MainActivity.goToLEDVIew.setVisibility(View.INVISIBLE);
        Log.d("LED_SCREEN", "onDestroy() called");
    }

    private View.OnClickListener ledListener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            final ImageButton btn = (ImageButton) v;
            final String led = getResources().getResourceEntryName(btn.getId());
            if(colorPickerDialog == null) colorPickerDialog = new ColorPickerDialog(LedScreen.this, Color.RED);
            colorPickerDialog.setAlphaSliderVisible(false);
            colorPickerDialog.setHexValueEnabled(true);
            colorPickerDialog.setTitle("LED-Color: ");
            colorPickerDialog.setOnColorChangedListener(new ColorPickerDialog.OnColorChangedListener(){
                @Override
                public void onColorChanged(int color){
                    // TODO: IF led == all
                    if(led.equals("all_led")){
                        led1.setBackgroundColor(color);
                        led2.setBackgroundColor(color);
                        led3.setBackgroundColor(color);
                        led4.setBackgroundColor(color);
                        led5.setBackgroundColor(color);
                        allLeds.setBackgroundColor(color);
                    } else {
                        btn.setBackgroundColor(color);
                    }
                    // TODO: send data via bluetooth
                    if(btService != null){
                        String bytes = Integer.toHexString(color);
                        byte[] bytesToSend = hexStringToByte(bytes);
                        btService.writeColor(led, bytesToSend);
                        Log.d("BT", "write bytes to microcontroller");
                    }

                }
            });
            colorPickerDialog.show();
        }
    };

    private byte[] hexStringToByte(String s){
        int len = s.length();
        if(len % 2 != 0){
            byte[] data = new byte[2];
            data[0] = 0;
            data[1] = 0;
            return data;
        }

        byte[] data = new byte[len/2];
        for(int i = 0; i < len; i+=2){
            data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }
}
