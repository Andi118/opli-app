package de.openlicht.openlicht;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 * Created by darke on 13.02.2018.
 */

public class BluetoothConnectionService {

    private static final String appName = "OpenLicht";

    private static final UUID MY_UUID_SPP = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    private final BluetoothAdapter mBluetoothAdapter;
    Context mContext;

    private AcceptThread mInsecureAcceptThread;
    private ConnectThread mConnectThread;
    private BluetoothDevice mDevice;
    private UUID deviceUUID;
    ProgressDialog mProgressDialog;

    private ConnectedThread mConnectedThread;


    public BluetoothConnectionService(Context context, BluetoothAdapter bluetoothAdapter){
        mContext = context;
        mBluetoothAdapter = bluetoothAdapter;
        start();
    }

    public UUID getUUID(){ return MY_UUID_SPP; }

    public void testPrint(){
        Log.d("BT-Service", "Hello");
    }

    private class AcceptThread extends Thread {

        private final BluetoothServerSocket mServerSocket;

        public AcceptThread(){
            BluetoothServerSocket tmp = null;

            try {
                tmp = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(appName, MY_UUID_SPP);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mServerSocket = tmp;
        }

        @Override
        public void run(){
            Log.d("BT", "run: running");

            BluetoothSocket socket = null;

            try {
                socket = mServerSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(socket != null){
                connected(socket, mDevice);
            }
        }

        public void cancel(){
            try{
                mServerSocket.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public class ConnectThread extends Thread{
        private BluetoothSocket mSocket;

        public ConnectThread(BluetoothDevice device, UUID uuid){
            mDevice = device;
            deviceUUID = uuid;
        }

        public void run(){
            BluetoothSocket tmp = null;

            try {
                tmp = mDevice.createRfcommSocketToServiceRecord(deviceUUID);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mSocket = tmp;
            mBluetoothAdapter.cancelDiscovery();

            try {
                mSocket.connect();
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    mSocket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            connected(mSocket, mDevice);
        }

        public void cancel(){
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void start(){
        if(mConnectThread != null){
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if(mInsecureAcceptThread == null){
            mInsecureAcceptThread = new AcceptThread();
            mInsecureAcceptThread.start();
        }
    }

    public void startClient(BluetoothDevice device, UUID uuid){
        mProgressDialog = ProgressDialog.show(mContext, "Connecting Bluetooth", "Connecting...", true);
        mConnectThread = new ConnectThread(device, uuid);
        mConnectThread.start();
    }

    private class ConnectedThread extends Thread {

        private final BluetoothSocket mSocket;
        private final InputStream mInputStream;
        private final OutputStream mOutputStream;

        public ConnectedThread(BluetoothSocket socket){
            mSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            mProgressDialog.dismiss();

            try {
                tmpIn = mSocket.getInputStream();
                tmpOut = mSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            mInputStream = tmpIn;
            mOutputStream = tmpOut;
        }

        public void run(){
            byte[] buffer = new byte[1024];
            int bytes;

            while(true){
                try {
                    bytes = mInputStream.read(buffer);
                    String incomingMessage = new String(buffer, 0, bytes);
                    Log.d("ConnectedThread", "InputStream: " + incomingMessage);
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }

        public void cancel(){
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void write(byte[] bytes){

            // TODO: send data to LED-Board
            String text = new String(bytes, Charset.defaultCharset());
            try {
                mOutputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void connected(BluetoothSocket mSocket, BluetoothDevice mDevice) {
        mConnectedThread = new ConnectedThread(mSocket);
        mConnectedThread.start();
    }

    public void disconnect(){
        mConnectedThread.cancel();
    }

    public void write(byte[] out){
        mConnectedThread.write(out);
    }

    public void writeColorsFromPicture(byte[] out){
        if(buffer_8Bit == null) buffer_8Bit = new byte[16];
        buffer_8Bit[0] = (byte) 0x48;

        buffer_8Bit[1] = out[3];
        buffer_8Bit[2] = out[2];
        buffer_8Bit[3] = out[1];

        buffer_8Bit[4] = out[7];
        buffer_8Bit[5] = out[6];
        buffer_8Bit[6] = out[5];

        buffer_8Bit[7] = out[11];
        buffer_8Bit[8] = out[10];
        buffer_8Bit[9] = out[9];

        buffer_8Bit[10] = out[15];
        buffer_8Bit[11] = out[14];
        buffer_8Bit[12] = out[13];

        buffer_8Bit[13] = out[19];
        buffer_8Bit[14] = out[18];
        buffer_8Bit[15] = out[17];

        write(buffer_8Bit);

        String debug = "";
        for(int i = 0; i < buffer_8Bit.length; i++){
            debug += (Byte.toString(buffer_8Bit[i]) + ", ");
        }

        Log.d("BUFFER", debug);
    }

    private byte[] buffer_8Bit;
    private byte[] buffer_test;

    public void dimmColor(int r, int g, int b){
        if(buffer_test == null) buffer_test = new byte[9];
        buffer_test[0] = 0x49;
        buffer_test[1] = (byte)200;
        buffer_test[2] = (byte)30;

        buffer_test[3] = (byte)((b >> 8) & 0x1);
        buffer_test[4] = (byte)(b & 0xFF);

        buffer_test[5] = (byte)((g >> 8) & 0x1);
        buffer_test[6] = (byte)(g & 0xFF);

        buffer_test[7] = (byte)((r >> 8) & 0x1);
        buffer_test[8] = (byte)(r & 0xFF);
    }

    public void writeColor(int color1, int color2, int color3, int color4, int color5){
        if(buffer_8Bit == null) buffer_8Bit = new byte[16];

        /* Color a r g b => color1 >> 8 (R) color1 >> 4 (G) color1 */

        /* Das ist unschön, finde eine "schönere" Lösung */
        setBuffer(8, false, 1, (byte)((color1 >> 8) & 0xFF), (byte)((color1 >> 4) & 0xFF), (byte)((color1) & 0xFF));
        setBuffer(8, false, 4,(byte)((color2 >> 8) & 0xFF), (byte)((color2 >> 4) & 0xFF), (byte)((color2) & 0xFF));
        setBuffer(8, false, 7, (byte)((color3 >> 8) & 0xFF), (byte)((color3 >> 4) & 0xFF), (byte)((color3) & 0xFF));
        setBuffer(8, false, 10, (byte)((color4 >> 8) & 0xFF), (byte)((color4 >> 4) & 0xFF), (byte)((color4) & 0xFF));
        setBuffer(8, false, 13, (byte)((color5 >> 8) & 0xFF), (byte)((color5 >> 4) & 0xFF), (byte)((color5) & 0xFF));

    }

    public void writeColor(String led, byte[] color){

        if(buffer_8Bit == null) buffer_8Bit = new byte[16];
        int R = color[1] & 0xFF;
        int G = color[2] & 0xFF;
        int B = color[3] & 0xFF;

        buffer_8Bit[0] = (byte) 0x48;

        switch(led){
            case "led1":
                setBuffer(8, false, 1, R, G, B);
                write(buffer_8Bit);
                break;
            case "led2":
                setBuffer(8, false, 4, R, G, B);
                write(buffer_8Bit);
                break;
            case "led3":
                setBuffer(8, false, 7, R, G, B);
                write(buffer_8Bit);
                break;
            case "led4":
                setBuffer(8, false, 10, R, G, B);
                write(buffer_8Bit);
                break;
            case "led5":
                setBuffer(8, false, 13, R, G, B);
                write(buffer_8Bit);
                break;
            case "all_led":
                setBuffer(8, false, 1, R, G, B);
                setBuffer(8, false, 4, R, G, B);
                setBuffer(8, false, 7, R, G, B);
                setBuffer(8, false, 10, R, G, B);
                setBuffer(8, false, 13, R, G, B);
                write(buffer_8Bit);
                break;
            default:
                break;
        }
        /* FOR DEBUG */
        String debug = "";
        for(int i = 0; i < buffer_8Bit.length; i++){
            debug += (Byte.toString(buffer_8Bit[i]) + ", ");
        }

        Log.d("BUFFER", debug);
    }

    private void setBuffer(int size, boolean isEven, int startIndex, int R, int G, int B){
        switch(size){
            case 8:
                buffer_8Bit[startIndex + 0] = (byte) B;
                buffer_8Bit[startIndex + 1] = (byte) G;
                buffer_8Bit[startIndex + 2] = (byte) R;
                break;
            default:
                break;
        }
    }
}
