package de.openlicht.openlicht;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;

import java.util.ArrayList;

public class LedListAdapter extends ArrayAdapter<BluetoothDevice> {

    private LayoutInflater mLayoutInflater;
    private ArrayList<BluetoothDevice> mDevices;
    private int mViewResourceId;
    private MainActivity instance;

    public LedListAdapter(Context context, int tvResourceId, ArrayList<BluetoothDevice> devices, MainActivity instance){
        super(context, tvResourceId, devices);
        this.mDevices = devices;
        this.instance = instance;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = tvResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        convertView = mLayoutInflater.inflate(mViewResourceId, null);

        Button ledButton = convertView.findViewById(R.id.ledColorButton);
        SeekBar dimmer = convertView.findViewById(R.id.dimmer);

        ledButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("LED-VIEW", "Color-Button pressed");
                Button btn = (Button) view;
                btn.setBackgroundColor(Color.RED);
            }
        });

        return convertView;
    }
}
