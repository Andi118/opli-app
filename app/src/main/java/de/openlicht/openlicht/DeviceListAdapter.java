package de.openlicht.openlicht;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by darke on 13.02.2018.
 */

public class DeviceListAdapter extends ArrayAdapter<BluetoothDevice>{

    private LayoutInflater mLayoutInflater;
    private ArrayList<BluetoothDevice> mDevices;
    private int mViewResourceId;
    private MainActivity instance;

    public DeviceListAdapter(Context context, int tvResourceId, ArrayList<BluetoothDevice> devices, MainActivity instance){
        super(context, tvResourceId, devices);
        this.mDevices = devices;
        this.instance = instance;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = tvResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        convertView = mLayoutInflater.inflate(mViewResourceId, null);

        final BluetoothDevice device = mDevices.get(position);

        if(device != null){
            TextView deviceName = convertView.findViewById(R.id.tvDeviceName);
            final Button connectBtn = convertView.findViewById(R.id.connectBtn);

            if(deviceName != null){
                deviceName.setText(device.getName());
                String btnText;
                if(device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    btnText = "Connect";
                }
                else{
                    btnText = "Pair";
                }
                connectBtn.setText(btnText);

                connectBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(connectBtn.getText() == "Connect"){
                            Log.d("CONNECT", "in progress...");
                            instance.connectToBT(device);
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                device.createBond();
                                if(device.getBondState() == BluetoothDevice.BOND_BONDED){
                                    connectBtn.setText("Connect");
                                }
                            }
                        }
                    }
                });
            }
        }

        return convertView;
    }
}
