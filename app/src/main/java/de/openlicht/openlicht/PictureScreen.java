package de.openlicht.openlicht;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by darke on 15.02.2018.
 */

public class PictureScreen extends Activity {

    private File imageFile;
    private ImageView imageView;
    private Button tpButton, c1, c2, c3, c4, c5, getColors;
    private int[] color = new int[5];
    private int tmpColor = 0;
    private int index = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_layout);
        tpButton = findViewById(R.id.takePictureBtn);

        c1 = findViewById(R.id.color1);
        c2 = findViewById(R.id.color2);
        c3 = findViewById(R.id.color3);
        c4 = findViewById(R.id.color4);
        c5 = findViewById(R.id.color5);
        c1.setBackgroundColor(Color.BLACK); c2.setBackgroundColor(Color.BLACK); c3.setBackgroundColor(Color.BLACK);
        c4.setBackgroundColor(Color.BLACK); c5.setBackgroundColor(Color.BLACK);

        getColors = findViewById(R.id.getColors);

        getColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("color", color);
                setResult(5, intent);
                for(int i = 0; i < color.length; i++)
                    Log.d("COLOR TO GET", "C: " + color[i]);
                finish();
            }
        });

        c1.setOnClickListener(colorListener);
        c2.setOnClickListener(colorListener);
        c3.setOnClickListener(colorListener);
        c4.setOnClickListener(colorListener);
        c5.setOnClickListener(colorListener);

        imageView = findViewById(R.id.imageView);
        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int action = motionEvent.getAction();
                final int evX = (int) motionEvent.getX();
                final int evY = (int) motionEvent.getY();

                switch(action){
                    case MotionEvent.ACTION_DOWN:
                        Log.d("PICTURE", "onTouch: down");
                        imageView.setDrawingCacheEnabled(true);
                        Bitmap imgbmp = Bitmap.createBitmap(imageView.getDrawingCache());
                        imageView.setDrawingCacheEnabled(false);
                        int pxl = imgbmp.getPixel(evX, evY);
                        tpButton.setBackgroundColor(pxl);
                        tmpColor = pxl;
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.d("PICTURE", "onTouch: up");

                        break;
                }
                return true;
            }
        });
    }

    private View.OnClickListener colorListener = new View.OnClickListener(){
        @Override
        public void onClick(View v){
            final Button btn = (Button) v;
            final String led = getResources().getResourceEntryName(btn.getId());
            if(led.equals("color1")) {
                index = 0;
                Log.d("INDEX", "set to " + index);
            }
            if(led.equals("color2")){
                index = 1;
                Log.d("INDEX", "set to " + index);
            }
            if(led.equals("color3")){
                index = 2;
                Log.d("INDEX", "set to " + index);
            }
            if(led.equals("color4")){
                index = 3;
                Log.d("INDEX", "set to " + index);
            }
            if(led.equals("color5")){
                index = 4;
                Log.d("INDEX", "set to " + index);
            }
            v.setBackgroundColor(tmpColor);
            color[index] = tmpColor;
        }
    };

    private Uri cameraPicUri;

    public void process(View view){
        final CharSequence[] items = {"Camera", "Gallery", "Cancel"};

        Log.d("PICTURE", "clicked");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(items[i].equals("Camera")){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                    File picDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    String picName = createPictureName();
                    File picFile = new File(picDir, picName);
                    Uri picUri = Uri.fromFile(picFile);
                    cameraPicUri = picUri;
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraPicUri);
                    startActivityForResult(intent, 1);

                } else if(items[i].equals("Gallery")){
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Select File"), 2);
                } else if(items[i].equals("Cancel")){
                    dialogInterface.dismiss();
                }
            }
        });

        builder.show();
    }

    private String createPictureName(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timestamp = sdf.format(new Date());

        return "opli_" + timestamp + ".jpg";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == 1){
                decodeUri(cameraPicUri);
                Log.d("CAMERA", "picture loaded");

                /*
                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                imageView.setImageBitmap(bmp); */
            } else if(requestCode == 2){
                decodeUri(data.getData());
            }
        }
    }

    public void decodeUri(Uri uri) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, o);

            // the new size we want to scale to
            final int REQUIRED_SIZE = 1024;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(imageSource, null, o2);

            imageView.setImageBitmap(bitmap);

        } catch (FileNotFoundException e) {
            // handle errors
        } catch (IOException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
    }
}
